# Angular Tech Test

We would like you to create a new Angular project which pulls in data from the Random User API (https://randomuser.me/documentation#howto).

The application should display the full name, DOB and email address of each user in a human-readable format.

The application should provide a way for the user to filter the data by Surname and DOB through the UI.

Please add tests for the functionality, time permitting.

## Guidance for Candidate

- Close any existing repositories or code you may have open
- Feel free to use Google - we’re interested in your thought process and seeing how you find the answers that you need.
- The architecture of the project is entirely your choice.
- We would be interested in knowing how you would improve and expand upon the project given ample time.
- Don’t worry about styling.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.
